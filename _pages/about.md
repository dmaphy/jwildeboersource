---
title: "About/Impressum/GDPR"
layout: single
excerpt: "Information about this blog, GDPR/DSGVO compliance information, Impressum"
author_profile: true
search: false
permalink: /about/
header:
  image: mh/mh009.jpg
---

## About

Das ist mein persönliches Blog, wo ich frei und offen über meine Meinungen rede, über Themen poste die ich spannend finde. Die hier geposteten Artikel spiegeln deshalb nur meine persönliche Meinung wider und sind in keinster Weise mit meiner Arbeit bei Red Hat verbunden.

This is my private blog where I post things I care about, explain stuff I think is worth knowing about. This blog thus solely expresses my private, personal opinions and is not representative of my work at Red Hat. 

## Impressum

**Angaben gemäß § 5 TMG  
Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV**

Jan Wildeboer  
Heilwigstraße 91  
81827 München  
GERMANY

### Contact:

E-Mail: blog@wildeboer.net

### Datenschutzerklärung (DSGVO)

Auf dieser Webseite werden keine personenbezogenen Daten erhoben, gespeichert oder verarbeitet. Es werden keine Cookies verwendet.

### Data Protection Policy (GDPR)

This website does not collect, store or process Personally Identifiable Information (PII). This website does not require the use of cookies.

### Verweis auf Dritte

Diese Website wird auf [Codeberg](https://codeberg.org) als [Codeberg Pages](https://pages.codeberg.org) gehostet. Die Codeberg Privacy Policy ist [hier](https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md) hinterlegt.

### Third Party Inclusion
This website is hosted by [Codeberg](https://codeberg.org) using [Codeberg Pages](https://pages.codeberg.org)). The Codeberg Privacy Policy is available [here](https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md)
