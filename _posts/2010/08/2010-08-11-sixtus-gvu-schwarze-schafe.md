---
id: 1279
title: Sixtus, GVU, Schwarze Schafe
date: 2010-08-11T09:19:28+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1279
permalink: /2010/08/sixtus-gvu-schwarze-schafe/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Die Kurzform: [Sixtus](http://sixtus.cc) macht tolle Filme/Spots. Er veröffentlicht diese Filme auf [vimeo.de](http://www.vimeo.de). Aber da gibt es auch noch die [GVU](http://www.gvu.de/1_Startseite.htm), die im Namen der Gerechtigkeit gnadenlos die Verletzer von Urheberrechten verfolgt.

Wenn das zusammenkommt, passiert Erstaunliches: Einer der offiziell beauftragten Handlanger der GVU ist der Überzeugung das die Filme von Sixtus auf vimeo.de irgendwie gegen das Urheberrecht verstossen.

Man hat ja Ahnung, und deshalb ist es scheinbar auch überflüssig den wirklichen Urheber &#8211; Sixtus &#8211; mal kurz zu fragen &#8211; oder wenigstens nach getaner Arbeit zu informieren.

Nettoresultat: vimeo kriegt eine Aufforderung ein paar Folgen vom Elektrischen Reporter zu löschen. Akt 2 der unglaublichen Geschichte &#8211; vimeo nimmt die Filme runter &#8211; ohne anscheinend selbst nochmal zu prüfen.

Eklatante Fehler meiner Meinung nach. Sixtus sieht das wohl ähnlich und kämpft dagegen. Er kämpft um das Recht seine eigenen Werke weiterhin zu veröffentlichen. Schilda?

Nun macht aber die GVU auch gute Sachen &#8211; es gibt einen Wettbewerb [&#8220;Das Schwarze Schaf&#8221;](http://dasschwarze-schaf.de/) &#8211; da kann man Vorschläge machen für &#8220;die dreisteste Rechtsverletzung im Internet&#8221;. Und genau das habe ich gemacht. Ich habe die GVU selbst als Kandidat vorgeschlagen:

> Vielen Dank für Ihre Teilnahme!
> 
> Ihr Formular wurde an uns abgeschickt! Folgende Daten wurden uns übermittelt:
> 
> - Datum &#8211; Zeit: 11.08.2010 07:37:53
> - Absender-IP: 209.132.186.12
> - Name: Jan Wildeboer
> - Firma: Privat  
> - Email: jan.wildeboer@gmx.de
> - Telefon: +491743323XXX
> - Adresse: Hirschgartenallee, 33b 80639 München
> - Verkäufer Name: GVU
> - Verkäufer-Plattform: u.A. auf vimeo.de schlägt dieser dreiste Verein gnadenlos zu!
> - Verkäufer-Schaden: Den Glauben an Respekt vor dem Urheberrecht und seiner Durchsetzung
> - Vorgangsbeschreibung:
>
> Wie mehrfach gemeldet, hat die GVU über einen Dritten Filme bei vimeo.de sperren lassen &#8211; OBWOHL diese Filme vom Urheberrechtsinhaber SELBST dort eingestellt wurden. Ohne Auftrag des Urhebers, ja sogar ohne dessen Wissen!
> 
> http://sixtus.cc/in-sachen-gvu
>  
> Wo sind sie auf uns aufmerksam geworden:
> 
> - identi.ca via @floeff
> 

Mal schauen ob die GVU diesen Preis gewinnt. Wer sich mir anschliessen will, kann auch gerne selbst einen Vorschlag einreichen. einfach hier:
 
[http://www.dasschwarze-schaf.de/html/schwarzes\_schaf\_melden.html](http://www.dasschwarze-schaf.de/html/schwarzes_schaf_melden.html)
