---
id: 1274
title: 'On Contributions &#8211; The GNOME Affair'
date: 2010-07-30T10:31:46+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1274
permalink: /2010/07/on-contributions-the-gnome-affair/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
The discussion is quite on the roll after Dave Neary gave some [insight](http://blogs.gnome.org/bolsh/2010/07/28/gnome-census/) into who commits how much code to the GNOME project.

As explained [here](http://gregdekspeaks.wordpress.com/2010/07/29/red-hat-16-canonical-1/) by Greg de Koenigsberg, Red Hat has outperformed Canonical on a 16:1 ratio according to the Census. You can imagine that some of the Ubuntu fans don&#8217;t like to hear this.

So after Jeffrey Stedfast puts out his [reply](http://jeffreystedfast.blogspot.com/2010/07/re-red-hat-16-canonical-1.html), we now also have [Jono Bacon](http://www.jonobacon.org/2010/07/30/red-hat-canonical-and-gnome-contributions/#comment-152318) stepping in.

Jono&#8217;s main argument is that Canonical does a lot on top of GNOME, but on their own, using their own tools and build environment. And he calls this &#8220;contributing&#8221;.

I beg to differ.

A contribution in my view is something that ends up in the upstream project. Something that is developed outside of the project is NOT a contribution TO the project. It merely stands on the shoulders of giants, in this case the GNOME project, but it doesn&#8217;t add to the upstream project itself.

This is like kernel modules that are developed outside of the linux tree &#8211; they may run on Linux, but you cannot consider them to be PART of Linux.

So if you accept that contribution means &#8220;adding something TO the upstream project so that it is an integral part of it&#8221; the argument of Greg still holds strong. So far Red Hat has contributed 16x more to GNOME as Canonical.

Just wanted to make this clear.
