---
id: 1333
title: 'Congressional Research Service on wikileaks and prosecution #mustread'
date: 2010-12-14T13:49:23+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1333
permalink: /2010/12/congressional-research-service-on-wikileaks-and-prosecution-mustread/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
[Here](http://fpc.state.gov/documents/organization/152053.pdf) is a very interesting document titled &#8220;Criminal Prohibitions on the Publication of Classified Defense Information&#8221; by Jennifer K. Elsea, Legislative Attorney from October 18, 2010. It gives you insight into how the US is struggling to find ways to prosecute Julian Assange for leaking documents. The summary says it all:

> The recent online publication of classified defense documents by the organization WikiLeaks and subsequent reporting by the New York Times and other news media have focused attention on whether such publication violates U.S. criminal law. The Attorney General has reportedly stated that the Justice Department and Department of Defense are investigating the circumstances to determine whether any prosecutions will be undertaken in connection with the disclosure.
> 
> This report identifies some criminal statutes that may apply, but notes that these have been used almost exclusively to prosecute individuals with access to classified information (and a corresponding obligation to protect it) who make it available to foreign agents, or to foreign agents who obtain classified information unlawfully while present in the United States. Leaks of classified information to the press have only rarely been punished as crimes, and we are aware of
  
> no case in which a publisher of information obtained through unauthorized disclosure by a government employee has been prosecuted for publishing it. There may be First Amendment implications that would make such a prosecution difficult, not to mention political ramifications based on concerns about government censorship. To the extent that the investigation implicates any foreign nationals whose conduct occurred entirely overseas, any resulting prosecution may carry foreign policy implications related to the exercise of extraterritorial jurisdiction.
> 
> This report will discuss the statutory prohibitions that may be implicated, including the Espionage Act; the extraterritorial application of such statutes; and the First Amendment implications related to such prosecutions against domestic or foreign media organizations and associated individuals. 

I have just started reading the document and as IANAL I guess my interpretations are of no real use. I leave it to professionals to comment on it here or elsewhere with a baclink.
