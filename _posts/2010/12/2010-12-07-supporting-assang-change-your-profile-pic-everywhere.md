---
id: 1324
title: 'Supporting Assange &#8211; change your profile pic everywhere'
date: 2010-12-07T14:22:02+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1324
permalink: /2010/12/supporting-assange-change-your-profile-pic-everywhere/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Based on the PDF at http://www.justiceforassange.com/IMG/pdf/Assange_face.pdf I have created the following bitmap which I will now put up as profile picture on all social networks I am member of. Do the same too!

![Assange](/images/2010/12/Assange.png)
*Assange profile pic*

OR (with JUSTICE added):

![Assange](/images/2010/12/Assange1.png)
*Assange Justice Pic*

We can make a difference.
