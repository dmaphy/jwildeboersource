---
id: 711
title: '#ashmob Updates!'
date: 2010-04-23T19:55:14+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=711
permalink: /2010/04/ashmob-updates/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
ASHMOB RULES

What is #ashmob? Just for fun! We BBQ to send ashes to Iceland as our revenge for the flight disaster!

[Facebook Group &#8211; Join us!](http://www.facebook.com/group.php?gid=112053748827994&#038;v=app_2344061033&#038;ref=ts#!/group.php?gid=112053748827994&#038;ref=ts)

#1: Anyone can do #ashmob! Invite your friends, heat up the BBQ! Tell us! We will create an FB event and make you admin.
  
#2: If someone from Iceland comes to your #ashmob, he gets food for free! Take pix of passport!
  
#3: Use the logo, a better version is at https://jan.wildeboer.net. Print it, make T-Shirts, ta&#8230;ke pictures und upload them here!
  
#4 Inform your local TV, Radio, Newspaper! Promote it!
  
#5 Last but not least &#8211; have FUN! Send back ashes to Iceland!

[ASHMOB BBQ Berlin!](http://www.facebook.com/group.php?gid=112053748827994&#038;v=app_2344061033&#038;ref=ts#!/event.php?eid=116203641730814&#038;index=1)

Date: Saturday, April 24, 2010
  
Time: 2:00pm &#8211; 8:00pm
  
Location: Berlin, Germany
  
Street: Görlitzer Park

Hier trifft sich das Nützliche mit dem Vergnüglichen. Die Grillsaison hat bereits begonnen, da wollen auch wir unsere Marke setzen und laden alle Sympathisanten ein. Und alle Isländer bekommen eine Freiwurst!

[ASHMOB BBQ Cologne](http://www.facebook.com/event.php?eid=119799114702345&#038;index=1)

Date: Saturday, April 24, 2010
  
Time: 5:00pm &#8211; 10:00pm
  
Location: Poller Wiesen

[ASHMOB BBQ MASSA](http://www.facebook.com/event.php?eid=112469218787654&#038;index=1)

Date: Saturday, April 24, 2010
  
Time: 5:50pm &#8211; 10:50pm
  
Location: Giardino
  
Street: via santa lucia 2
  
City/Town: Massa Lombarda, Italy

[ASHMOB BBQ MUNICH](http://www.facebook.com/event.php?eid=112472905454243)

Date: Saturday, April 24, 2010
  
Time: 5:00pm &#8211; 10:00pm
  
Location: Flaucher, Munich, Germany

Wir warten nicht, wir starten. Wir grillen Asche nach Island. Mehr Details ASAP.

Let&#8217;s BBQ ash to Iceland. Details to follow ASAP
