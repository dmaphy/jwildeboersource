---
title: '#ashmob - BBQ ash back to iceland! (Just for fun)'
date: 2010-04-19T10:01:48+00:00
author: jwildeboer
layout: single
permalink: /2010/04/ashmob-bbq-ash-back-to-iceland-just-for-fun/
categories:
  - OpenMisc
---
After I am grounded due to the volcano ash, I decided to fight back. Coordinated BBQ all across the world! #ashmob! Here&#8217;s the logo.

![Ashmob Logo](/images/2010/04/ashmob.png)
*Ashmob logo*
  
And [here](http://www.facebook.com/group.php?gid=112053748827994) is the Facebook group where you can become member. Stay tuned! More to come!

## UPDATE years later

I have long left Facebook so the group will be gone by now. Good ;)