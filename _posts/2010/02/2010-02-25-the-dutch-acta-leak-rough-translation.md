---
id: 617
title: 'The dutch ACTA leak &#8211; rough translation'
date: 2010-02-25T14:33:18+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=617
permalink: /2010/02/the-dutch-acta-leak-rough-translation/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Original dutch source is [here](http://www.bigwobber.nl/2010/02/25/acta-verslagen-van-het-ministerie-van-economische-zaken/). I hunted it through google translate and cleaned up some of the translation mistakes. As I am a native dutch, I do know what I am correcting. However &#8211; this is NOT an official translation of any kind. My comments are marked in [ ].

**Report of Management Innovation week 49 from November 30 to December 4, 2009**

**ACTA (Official 1 &#8211; name known to editors)**
  
At the EU level currently two sections of ACTA are being discussed, namely civil enforcement of IP rights and enforcement of IP [Intellectual Property] on the Internet.

The last chapter [on Internet] is compiled by the U.S. and has only recently been provided.

Because the documents and ACTA negotiations are confidential, this chapter has caused various groups on the internet to speculate about the possible content (esp. wrt &#8216;three strikes out).

We have received two letters by dutch citizens on this subject: one of the Brein Foundation, in which Stas [State Secretary] has been asked to include all forms of piracy in the chapter and a letter from a citizen who, based on the Brein letter, asked to not include piracy in ACTA.

EU-questionnaires wrt study of information-sharing by public sector bodies in combating piracy and counterfeiting (1 Officer &#8211; name known to editors) asked public institutions IM DG (ministries and implementing agencies) of MS to provide insight wrt information exchange wrt to combat piracy and counterfeiting.

EUCie strives for an electronic information network, which authorities can use to exchange information on the above combatting.

Given the amount of players and EZ [Ministry of Economics] is the responsible ministry for enforcement of IP rights [Intellectual Property], the answering of this questionnaire is coordinated by EZ.

**INTER DEPARTMENTAL COUNCIL FOR TRADE POLICY IRHP 2010-02 (c)
  
** 
  
REPORT OF THE DUTCH DELEGATION TO
  
THE MEETING OF THE COMMERCIAL POLICY COMMITTEE HELD IN BRUSSELS DD 15 JANUARY 2010

**3. 3. ACTA: preparations of the 7th round of negotiations ACTA:**

Presidency requested to mark the participation of LS ASAP. Cie reported on latest developments regarding the issue of transparency.

Cie willing to go towards full disclosure of the documents, but it was dependent on the thoughts of the new Commissioner and of course
  
had to take into account the wishes of some third countries (U.S. seemed unconvinced).

Cie feared setting a precedent for the free trade agreements now being negotiated, it was not intended that the EU position would become completely public. This could have major consequences for EU interests.

POL [Poland], VK [United Kingdom], OOS [Austria], NL [Netherlands], FIN [Finland], IER [Ireland], HON [Hungary], EST [Estonia], ZWE [Sweden] were in favcour of more transparency.

FRA [France] did not object against full disclosure if that would be the consensus, but did have concerns about the U.S. position.

ITA [Italy] sided along with France, was also concerned about impacts on free trade agreements, noted that even if plurilateral setting the precedent of ACTA would in principle be adequate closure. DK agreed with ITA and put reserve study status on the documents.

HON [Hungary] however opposed this with the position that the treatment of ACTA documents would be much more logical to compare with the documents of multilateral negotiations.

Cie announced the next stakeholder should take place after the Mexico round.

Also it is to be avoided to set a precedent towards bilateral negotiations.

**INTER DEPARTMENTAL COUNCIL FOR TRADE POLICY IRHP 2010-06 (c)
  
REPORT OF THE DUTCH DELEGATION TO THE MEETING OF THE COMMERCIAL POLICY COMMITTEE HELD IN BRUSSELS DD 5 FEBRUARY 2010
  
** 

ACTA: 7th round done with good progress.

LS [Member States] wants Cie to pro-actively advocate transparency * (wrt publishing documents), but Cie does not agree. LS [Member States] put more pressure on this.

6. ACTA (7th round, Mexico)
  
Cie reports. Long meeting done, the parties still seemed unwilling to offer major concessions. Good progress on customs procedures (exception for personal lugage agreed, is important, due to sensitivities in public opinion). Civil enforcement remains difficult chapter, because all parties have different legal systems. Transit and exports are still not decided. In this meeting the internet chapter could not be fully addressed due to lack of time, pushed to next round (including digital rights management). In general, the coverage was still an issue (EU offensive interests).

Transparency was also discussed, but this was rather to address criticisms in public opinion that were inappropriate (such as personal baggage checks and 3-strike rule for illegal downloading, both not part of ACTA). Release of documents remains undecided (Korea and Singapore oppose, Japan now suddenly wants to, USA remains silent). A written report will follow. Cie also was willing to share its line [of arguments] to take with the LS [Member states] when talking to the press. There will also be a stakeholder meeting next month (preferably a football stadium so that all supporters of the criticism of ACTA could be invited, as Cie joked). Next round in New Zealand 12-16 April and the ninth round probably in Geneva just before the TRIPS Council (Around the 7th of June).

UK once again declared its supportfor full disclosure of the documents, noted the current position [of secrecy] in EU is hard to keep national parliaments (European Parliament), citizens and civil society should be informed, there was nothing to hide.

UK insisted Cie should take a pro-active stance and should try to convince other parties of the need to be transparent. Backed by FIN, FRA, NL, SWE, OOS, HON, DK, ITA, IER, POL, BEL, POR. NL Cie and Legal Service of the Council are asked for possible legal basis of ACTA under the Lisbon Treaty and the possible involvement of national parliaments to ratify. ITA and POR noted that GI &#8216;s as of yet came off worst in the negotiations.

JDvdR reported that legal basis depends on the final text, but with criminal sanctions in it the text would create a mixed agreement.

DUI [Germany] declared at the end of the meeting to have no position on the issue of transparency. UK found that there was a consensus and thus the EU position could be changed.

Presidency said that the party line was not to become isolated on this issue, and that still remains. There were still some differing opinions of MS [Member States]. BE, POR, DK en DUI [Germany] are not sure that full transparency should be given. 

Seems that DUI, BE and POR can be convinced, but DK is not willing to change. The internal opinion finding process was not completed, but has to be for the next round in NZ, according to presidency.
