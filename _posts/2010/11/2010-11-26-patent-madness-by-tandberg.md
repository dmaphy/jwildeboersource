---
id: 1317
title: '[UPDATED] Patent Madness by Tandberg?'
date: 2010-11-26T12:12:36+00:00
modified: 2010-11-28
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1317
permalink: /2010/11/patent-madness-by-tandberg/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
tags:
  - Patents
---
Meet Lars Petter Endresen, Senior Engineer R&#038;D at [Tandberg](http://www.tandberg.com/). He is the official &#8220;inventor&#8221; named in this [patent application:](http://www.wipo.int/pctdb/en/wo.jsp?WO=2010077148&#038;IA=NO2009000447&#038;DISPLAY=STATUS)

![Tandberg Patent](/images/2010/11/TandbergPat.png "The Tandberg Patent" )
*The Tandberg Patent*

Business as usual. Strange claims on a very abstract level. The wonderful world of software patents.

BUT

In this case something strange is going on. Meet [Jason Garett-Glaser](http://x264dev.multimedia.cx/about), the primary developer of [x264](http://www.videolan.org/developers/x264.html), a free h264/avc encoder.

Jason is not amused, to say the least. <del datetime="2010-11-28T09:29:31+00:00">He <a href="http://x264dev.multimedia.cx/archives/589">claims</a> that this patent effectively is a translation of his code commit to patent chinese. And his arguments backing the claim are quite impressive.</del>

<del datetime="2010-11-28T09:29:31+00:00">So according to the published information we have right now, it seems that Mr. Endresen filed a patent on something he didn&#8217;t create nor implemented.</del> This could result in the true inventor being forced to remove his own code from his own software to avoid patent infringement. How weird is that?

This is patent business as usual. Unfortunately. While the politicians all across this planet are discussing stricter regulations on so-called plagiarism, infringement etc (think ACTA) this specific form of taking someone elses work and claiming to be its inventor is perfectly legal.

And you still ask why I personally think the patent system has severe problems? 😉

### UPDATE 2010-11-28

Jason tells the world in an update to his blog entry that he might have jumped to conclusions a bit too fast. This doesn&#8217;t change my arguments that there is a problem with the patent system though. A [similar case](http://slashdot.org/submission/1398854/My-GPL-code-has-been-patented) has sprung up almost at the same time. And we all remember the [SPF story](http://www.eweek.com/c/a/Security/Microsoft-Patent-Could-Hamper-EMail-Authentication-Group/) (Sender Permitted From) which also shows the same pattern.
