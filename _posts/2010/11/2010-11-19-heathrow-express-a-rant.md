---
id: 1311
title: 'Heathrow Express &#8211; a rant'
date: 2010-11-19T11:37:06+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1311
permalink: /2010/11/heathrow-express-a-rant/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
No reply so far.

> From: &#8220;Jan Wildeboer&#8221; jwildebo@[redacted]
>  
> To: web\_customer\_correspondence@[redacted]
>  
> Subject: Heathrow Express
>  
> Date: Thu, 18 Nov 2010 12:10:12 -0500
>  
> Message-ID: <[redacted]>
> 
> Dear sirs or madams,
> 
> Sitting on the heathrow express in the carriage with a sign that says &#8220;quiet
> zone&#8221; next to someone watching TV on his iPhone with the built-in speakers
> as the poor chap seemingly has no money to buy headphones, two people
> phoning quite loud on the other side of the aisle is not exactly what I
> expected.
> 
> I fully understand the communication needs of all of these VIPs flying in
> and out of Heathrow, but as a foreigner I try to respect the rules that are
> shown to me.
> 
> Please educate me on what I am dong wrong. Is it some sort of insider joke
> to phone in the quiet zone? Is it some kind of irony I don&#8217;t get? Or just an
> indication that the quiet zone effectively means the opposite?
> 
> My idea: make another carriage the &#8220;loud zone&#8221; with speakers built in to
> every seat, flashy lights everywhere.
> 
> Sincerely yours
> 
> Jan wildeboer
