---
id: 1298
title: '#possesa &#8211; POSSE South Africa Day 1'
date: 2010-10-04T14:21:34+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1298
permalink: /2010/10/possesa-posse-south-africa-day-1/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
tags:
  - POSSE
---
So here we are, all in a room, running POSSE. Capetown, it&#8217;s warm, it&#8217;s sunny, it&#8217;s a very nice place to be. Following the [schedule](http://teachingopensource.org/index.php/POSSE_South_Africa) as planned.

A lot of knowledge and information has been thrown in the face of our participants &#8211; and they cope remarkably well. Meeting people that are open for new tools, methods and are clearly able to put them in their own context is wonderful. You see the participants already thinking of ways to use the stuff we are doing in their own institutions.

But we have a lot to do in the next days. And as it seems, we have created a lot of motivation for exactly that! Some of the tools we discussed today:

  * IRC &#8211; the godfather of (near) realtime communication
  * Mailing lists &#8211; If it aint got posted, it doesn&#8217;t exist!
  * Wiki &#8211; Universal solution to group work
  * Etherpad &#8211; collaborate without hassle

We will have a lot more coming up, so stay tuned, follow us on twitter/identiu.ca at #posseSA &#8211; Let&#8217;s go!
