---
id: 658
title: 'Competition is fine &#8211; if facts stay facts.'
date: 2010-03-22T13:12:56+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=658
permalink: /2010/03/competition-is-fine-if-facts-stay-facts/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
So over at Red Hat, we are [quite proud](http://www.europe.redhat.com/news/article/2985.html) that IBM has chosen KVM via Red Hat Enterprise Virtualization to run their IBM Cloud as they [announce here:](http://www-03.ibm.com/press/us/en/pressrelease/29685.wss)

> [&#8230;]The new open cloud environment includes support for Linux &#8212; through Red Hat Enterprise Linux and SUSE Linux Enterprise from Novell &#8212; and Java. **Smart Business Development and Test on the IBM Cloud is powered by Red Hat Enterprise Virtualization, the Red Hat branded and supported KVM offering.** The enterprise cloud allows clients to work with their own images as well as images from IBM Mashup Center, Lotus Forms Turbo, WebSphere Portal Server, Lotus Web Content Management, and IBM Information Management and WebSphere brands that can be configured per their selection.[&#8230;]

You can imagine how proud we are that &#8221; &#8230; the IBM Cloud is powered by Red Hat Enterprise Virtualization, the Red Hat branded and supported KVM offering.&#8221; &#8211; this really shows how strong KVM is becoming in the virtualization market.

Another important point is that the IBM cloud &#8220;includes support for Linux &#8212; through Red Hat Enterprise Linux and SUSE Linux Enterprise from Novell&#8221;. So you can run virtual machines with both Red Hat and SuSE &#8211; just as you prefer. Ofcourse I am biased and would advise to use Red Hat, but that is not my point.

IBMs cloud is powered by Red Hat Enterprise Virtualization to run virtual machines. That&#8217;s the fact.

So why does [Michael Applebaum](http://www.novell.com/communities/user/3655), Senior Solutions Manager at SuSE [say this](http://www.novell.com/prblogs/?p=2079):

> So where does open source software (OSS) fit in? For one thing, it’s ideally positioned to provide the infrastructure for public and private cloud environments. This is a widely accepted view today, and we see it taking shape with (for example) our announcement that **SUSE Linux Enterprise Server is powering IBM’s** new development and test **cloud**.

Mr. Applebaum, for the sake of fairness, please do correct your statement. SuSE Linux Enterprise Server is a fully accepted guest in the IBM cloud, but the cloud is powered by KVM via Red Hat Enterprise Virtualization. I hope we can agree here.
