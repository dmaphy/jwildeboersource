---
id: 647
title: 'Open Standards &#8211; redefined?'
date: 2010-03-16T12:42:04+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=647
permalink: /2010/03/open-standards-redefined/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
For years and years I am using and promoting the term Open Standards. And it has always been very clear what an Open Standard is and, more important, what it is not.

You can go through various defintions of Open Standards:

  * [Free Software Foundation Europe](http://www.fsfe.org/projects/os/def.en.html)
  * [European Interoperability Framework (v1)](http://ec.europa.eu/idabc/en/document/3473#finalEIF)
  * [Digistan](http://www.digistan.org/open-standard:definition)
  * [Danish Parliament](http://www.ft.dk/Samling/20051/beslutningsforslag/B103/index.htm)
  * [Bruce Perens](http://perens.com/OpenStandards/Definition.html)

And no matter what differences you find in those definitions, they all agree on some crucial points, the most important being the freedom to use and implement the standard without having to ask for permission or having to pay license fees for the use of an Open Standard.

The Freedom to Use and Implement is fundamental to Open Standards. This means that whatever so-called &#8220;Intellectual Property&#8221; like (software) patents etc might be involved in an Open Standard **must** be made available to any third party on a Royalty Free basis.

Which leads to a simple conclusion &#8211; if you have to pay for use/implementation of a standard, this standard is **NOT** an open standard.

If you agree this far, pay special attention to [this](http://trade.ec.europa.eu/doclib/docs/2010/february/tradoc_145768.pdf):

> Currently, the Chinese companies using technologies detained by European companies are not allowed to enter into negotiations on the amount of royalties due to the latter, when they use their **essential patents in the framework of open standards.** The situation is highly detrimental to European companies and their complaint has been reflected in the European Chamber of Commerce in China (EUCCC) &#8211; IPR Working Group’s Position Paper 2005. The Commission therefore urged the Chinese government to take action in order **to ensure that those royalties are duly paid by Chinese companies.**

Hartmut Pilch from FFII pointed my attention to this and added some valuable comments [here](http://eupat.ffii.org/05/10/eucn/).

Bottom line is &#8211; DG Trade, represented by [Mr. Luc Pierre Devigne](http://ec.europa.eu/staffdir/plsql/gsys_fonct.properties?pLang=EN&#038;pSernum=613065&#038;pUnite=71119), seems to use the term Open Standards in a way that is simply not compatible with the accepted definition of Open Standards. Royalty payments on Open Standards can simply not exist in my view.

So either Mr. Devigne made a little mistake by using the term Open Standards here OR this is the start of redefining Open Standards to mean the exact opposite. Could someone talk to Mr. Devigne and ask hoim for clarification? This is an important question.
