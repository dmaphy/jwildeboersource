---
id: 535
title: Why I will not sign the MySQL petition
date: 2010-01-05T12:19:53+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=535
permalink: /2010/01/why-i-will-not-sign-the-mysql-petition/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
**DISCLAIMER: This post is my PERSONAL opinion.**

I am quite sure that I will get a lot of angry flames for this post, but I am not going to sign the [petition to &#8220;save MySQL&#8221;](http://www.helpmysql.org/en/theissue/customerspaythebill).

I strongly believe in community. In transparency. In openness. I hoped that the fight of Monty Widenius would be about exactly that &#8211; making sure MySQL becomes the Open Source database engine for the Free world. Making sure a vivid, creative, open community of users, developers gather around MySQL and make it better.

It seems however that this is not what the petition is about. Here some quotes that leave me with mixed feelings:

http://www.helpmysql.org/en/theissue/gplisnottheanswer

> MySQL&#8217;s database server has traditionally been a product developed and maintained by a single company, not a community project depending on volunteers or on multiple vendors (like Linux).

Well &#8211; at least they don&#8217;t hide it. But the wording is misleading. It implies that an open source project is either run by a company **or** by volunteers. This is so wrong that I have a hard time not loosing my temper. Take a look at [AMQP](http://www.amqp.org/confluence/display/AMQP/About+AMQP). It is a quite good ecosystem around a defined standard. With several implementations. Some of them Open Source, some not. Or take a look at the Linux kernel. Most of the contributions come from people who are actually **paid** to do this work. Some of the devleopers are employed by Red Hat, some by IBM, Novell, Intel etc.

A real Open Source project is about making something better for the good of **all** users. Not only for customers. And distributing the development across several companies **reduces** the risk for every participating entity. So the more the merrier.

However, for MySQL it seems the world is different:

> Those who wanted to incorporate it into or enhance it with closed-source products without having to publish their entire derived work on GPL terms (which would practically preclude them from many commercial opportunities)

Let me tell you, this is not correct. Red Hat can pay its employees by generating revenue based on the GPL. I would say that Red Hat does quite well with that business model. Look at the [stock price](http://finance.yahoo.com/echarts?s=RHT#chart1:symbol=rht;range=1y;indicator=volume;charttype=line;crosshair=on;ohlcvalues=0;logscale=on;source=undefined).

And if MySQL would be so succesfull with their dual licensing strategy, why did they sell themselves to SUN anyway?

There is a lesson hidden here: If you sell something, you don&#8217;t own it any longer. MySQL is now SUNs business. And if SUN decides to sell themselves to Oracle &#8211; it is **their** business. If the MySQL founders and fanboys don&#8217;t like this, they simply shouldn&#8217;t have sold MySQL to SUN in the first instance.

So we have two possible solutions here. My preferred solution: Oracle takes over SUN and puts MySQL under a single license, the GPL. It also declares all relevant &#8220;Intellectual Property&#8221; to be licensed Royalty Free up- and downstream.

Second best solution: The (former) MySQL people do what Open Source always offers: Fork. Let the best implementation win.

The current plan offfered in the petition however is based on exclusiveness, something that I refuse to accept. And that is why I personally will not sign the petition the way it is.

To all that quote me, let me repeat again: **THIS IS MY PERSONAL OPINION.**
