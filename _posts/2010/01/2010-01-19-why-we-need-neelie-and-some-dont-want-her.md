---
id: 555
title: 'Why we need Neelie (and some don&#8217;t want her)'
date: 2010-01-19T16:27:53+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=555
permalink: /2010/01/why-we-need-neelie-and-some-dont-want-her/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Neelie Smit-Kroes, most famous for her decisions wrt Microsoft in the various competitive cases, is under dubious pressure:

As noted [here](http://www.europeanvoice.com/article/2010/01/kroes-off-her-game/66896.aspx), some german conservatives express strange doubts:

> Angelika Niebler, a German conservative, was one of several to express dissatisfaction, saying that MEPs had not got “many specific answers” from the commissioner-designate. “I don&#8217;t really feel I know in which [policy] direction you are going to go,” she said.
> 
> Andreas Schwab, a German from the centre-right, described Kroes&#8217;s answer to one of his questions as a “sort of yes and no”, while Doris Pack – like Schwab and Niebler, a German member of the European People&#8217;s Party – said she was “not very satisfied” with an answer she received.

This comes after her hearing last week. If you take a closer look at what Neelie told us in that hearing, I fail to see why it is wrong. However I fully understand that some other companies and organisations are alarmed. This is what she had to say wrt Open Standards (transcribed from the hearing which is ironically only accessible with non-open Microsoft stuff):

> 5- Digital society depends upon open standards and interoperability. And with this in mind, public organizations should practice what they promote. If they don&#8217;t use open standards, why should citizens? I will pursue this from local authorities up to european institutions. If public data such as maps, weather information and health advise is not interoperable, how can it be exploited in new ways? And for such reasons I want to explore new ways to develop ICT standards in Europe.

So Neelie wants to explore new ways, wants to enforce the use of open standards, wants to free public data. I am not going to tell you who might have a different opinion. But I am sure, my dear Lazyweb, that you know which company I am talking about.

So let&#8217;s hope for the best. Neelie was grilled again today by the parliament and she will hear tomorrow if she is accepted.

For the sake of Open Standards in Europe, I can only hope she&#8217;ll make it.
