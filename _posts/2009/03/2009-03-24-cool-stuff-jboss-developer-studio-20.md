---
id: 25
title: 'Cool stuff: JBoss Developer Studio 2.0'
date: 2009-03-24T10:30:17+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=25
permalink: /2009/03/cool-stuff-jboss-developer-studio-20/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Red Hat has released the new JBoss Developer studio. Find more details [here](http://blog.softwhere.org/archives/816) and [here](http://www.businesswire.com/portal/site/google/?ndmViewId=news_view&newsId=20090323005622&newsLang=en). Why do I think this is important? Well &#8211; first of all I am proud of the company I work for and I am proud of the products we release.

But i n this case it is more. If you see what is included at this price, you will see why I am so fascinated.

Let me just quote [Rich Sharples](http://blog.softwhere.org/):

> The second thing we’re announcing is a change to our developer subscription and the introduction of J[Boss Developer Studio &#8211; Portfolio Edition (or JBDS-PE)](http://www.jboss.com/products/devstudio/). This new suite reflects the fact that JBDS has grown from being just a Seam / Java EE focussed tool set to now cover the whole JBoss portfolio &#8211; specifically that means the addition of some pretty sophisticated tooling for SOA and Portal development.
> 
> JBDS-PE gives you access to JBDS and all the JBoss run-times you need to develop against, access to Red Hat Network for updates   &#8211; all for $99 / year.

See &#8211; that is Open Source. Ofcourse you can also get all components for free, that is not the problem. But if you want to work in an environment that saves you the trouble of getting it all set up in a decent way, an envirionment that allows you to easily deploy on your JBoss production systems &#8211; this is defintely worth the bucks.

Happy hacking!
