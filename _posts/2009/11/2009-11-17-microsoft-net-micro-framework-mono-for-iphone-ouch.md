---
id: 468
title: 'Microsoft, .NET Micro Framework, Mono for iPhone &#8211; Ouch?'
date: 2009-11-17T11:14:38+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=468
permalink: /2009/11/microsoft-net-micro-framework-mono-for-iphone-ouch/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
So this will be a minor rant. And as always, this is my pure personal opinion and not related to my job.

So Microsoft broke the stunning [news](http://port25.technet.com/archive/2009/11/16/microsoft-to-open-source-the-net-micro-framework.aspx): They are going to open source the .[NET Micro Framework](http://www.microsoft.com/netmf/default.mspx) under the [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0).

What does this mean? Well &#8211; at first glance it is all good news. All the patents covering the .NET Micro framework are nnow available

> perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section)

where the exceptions are clearly defined and boil down to &#8220;sue me and you&#8217;ve lost the license rights&#8221;, typical covenant language.

So why don&#8217;t we all sing and praise Microsoft for this brave step in the right direction? First &#8211; there is always doubt when it comes to Microsoft and Open Source. For darn good reasons, I dare say. Think of TomTom and the FAT patent dispute.

Secondly &#8211; and IMHO more important &#8211; I am quite sure that Novell will not be totally happy &#8211; which they won&#8217;t admit in public, I dare also say 😉

According to my sources, the [MONO](http://www.mono-project.com/Main_Page) project was under pressure to finally make some revenue and one of the possibilities was to produce MONO for an important platform that Microsoft didn&#8217;t want to support &#8211; the iPhone.

So we now have MONOTouch for iPhone, and suprise, it has a [price tag](http://monotouch.net/Store). Even better, it is not open source, accrding to the [FAQ](http://monotouch.net/FAQ#How_is_MonoTouch_Licensed.3f):

> How is MonoTouch Licensed?
> 
> MonoTouch is a commercial product based on the open source Mono project and is licensed on a per-developer basis.

I guess you can see the problem here. Imagine that the .NET Micro Framework is &#8220;good enough&#8221; for the iPhone. Imagine that the community will soonish deliver a free implementation for the iPhone &#8211; who will need MONOTouch?

Now, the MONO people will claim that ofcourse the Micro Framework is incomplete, completely different and of no real use. But nevertheless &#8211; Microsoft did something that could become a pain for MONO. Is that what a partnership with Microsoft means? 😉

Let&#8217;s wait and see.
