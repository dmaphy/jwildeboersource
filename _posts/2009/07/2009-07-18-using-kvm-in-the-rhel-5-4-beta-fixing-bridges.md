---
title: 'Using KVM in the RHEL 5.4 Beta &#8211; fixing bridges'
date: 2009-07-18T11:25:02+00:00
author: jwildeboer
layout: single
categories:
  - OpenKnowledge
  - Tips & Tricks & Fixes
---
When you use KVM in Rhel 5.4 Beta you will notice that there is no bridge setup to allow your virtual guests to directly connect to the local network.

However, it is easily fixed:

As we are using libvirt, we just need to do two steps, as outlined [here](http://wiki.libvirt.org/page/Networking#Bridged_networking_.28aka_.22shared_physical_device.22.29):

Step 1: Create the bridge script at /etc/sysconfig/network-scripts/ifcfg-br0

`DEVICE=br0<br />
BOOTPROTO=static<br />
TYPE=Bridge<br />
IPADDR=10.20.30.40<br />
NETMASK=255.255.255.0<br />
ONBOOT=yes<br />
NM_CONTROLLED=no`

As you can see, I use static IP config.

Step 2: hook up eth0 to the bridge and remove it&#8217;s IP config in /etc/sysconfig/network-scripts/ifcfg-eth0

`DEVICE=eth0<br />
BOOTPROTO=none<br />
ONBOOT=yes<br />
BRIDGE=br0<br />
NM_CONTROLLED=no<br />
TYPE=Ethernet`

Now restart the network and done. When you now create a new virtual machine with virt-manager, you can select to have it directly hooked up to the physical network.
