---
id: 355
title: '[Sarcasm] Microsofts 10-K &#8211; some comments'
date: 2009-07-31T15:42:56+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=355
permalink: /2009/07/sarcasm-microsofts-10-k-some-comments/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
tags:
  - OpenMinds
---
Interesting stuff.

http://sec.gov/Archives/edgar/data/789019/000119312509158735/d10k.htm#tx73014_3

> &#8220;Open source commonly refers to software whose source code is subject to
  
> a license allowing it to be modified, combined with other software and
  
> redistributed, subject to restrictions set forth in the license. A
  
> number of commercial firms compete with us using an open source business
  
> model by modifying and then distributing open source software to end
  
> users at nominal cost and earning revenue on complementary services and
  
> products. These firms do not bear the full costs of research and
  
> development for the software. Some of these firms may build upon
  
> Microsoft ideas that we provide to them free or at low royalties in
  
> connection with our interoperability initiatives.&#8221; 

[Oh no. This interop thingy and the EU. They force M$ to open up stuff
  
that then gets implemented in Open Source. Interop steals their revenue!
  
How dare they!]

> &#8220;We are devoting significant resources toward developing our own
  
> competing software plus services strategies including the Windows Azure
  
> Platform, our hosted computing platform designed to facilitate the
  
> rapid, flexible and scalable development of cloud-based services. It is
  
> uncertain whether these strategies will be successful. &#8220;

[Translated: We invest in the hype but we dunno if it will ever work.]

> &#8220;Similarly, the absence of harmonized patent laws makes it more
  
> difficult to ensure consistent respect for patent rights. Throughout the
  
> world, we actively educate consumers about the benefits of licensing
  
> genuine products and obtaining indemnification benefits for intellectual
  
> property risks, and we educate lawmakers about the advantages of a
  
> business climate where intellectual property rights are protected.&#8221;

[Translated: We lobby heavily for more patents and against open standards.]

> &#8220;The [European] Commission’s impact on product design may limit our
  
> ability to innovate in Windows or other products in the future, diminish
  
> the developer appeal of the Windows platform, and increase our product
  
> development costs. The availability of licenses related to protocols and
  
> file formats may enable competitors to develop software products that
  
> better mimic the functionality of our own products which could result in
  
> decreased sales of our products.&#8221;

[Wow. So forcing M$ to respect the valid laws of competition is painted
  
as &#8220;impact on product design&#8221; and interoperability is painted as
  
competitors are now able to destroy M$ revenue? Remember &#8211; M$ was found
  
guilty of breaking laws.]
