---
id: 130
title: Spam protection on my blog
date: 2009-05-23T12:41:10+00:00
modified: 2022-06-13
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/2009/05/spam-protection-on-my-blog/
permalink: /2009/05/spam-protection-on-my-blog/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
tags:
  - OpenKnowledge
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 108469174905495238
---
I have activated Akismet and Yawasp for my WordPress blog here. It works. Here the current numbers:

Akismet has protected your site from 632 spam comments 

Yawasp has stopped 614 birdbrained Spambots. 

This really saves me some headaches &#8230;.

## UPDATE 2022-06-13

Obviously, nowadays I am running this blog with static pages, comments are via Mastodon and the spammers are not a problem anymore. Static pages FTW!