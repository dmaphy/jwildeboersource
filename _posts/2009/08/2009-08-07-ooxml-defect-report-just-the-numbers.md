---
id: 369
title: 'OOXML defect report &#8211; just the numbers'
date: 2009-08-07T13:31:06+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=369
permalink: /2009/08/ooxml-defect-report-just-the-numbers/
aktt_tweeted:
  - 1
category: OpenMisc
---
So for all those people that still believe that OOXML is in good shape and might just need a few corrections. It might be a bit more complicated.

I am just reading the &#8220;IS 29500:2008 Defect Report Log [At Closure of the DCOR1 and FDAM1 Sets]&#8221;. This is a very interesting 809 pager. Note: The ODF Spec is around 700 pages. So the defect report for OOXML effectively outperforms ODF pagecountwise 😉

ATM just the numbers, so you get a feeling for what is going on (or should I say wrong?).

<table>
  <tr>
    <td>
      <strong>Status</strong>
    </td>
    
    <td>
      <strong>Count</strong>
    </td>
  </tr>
  
  <tr>
    <td>
      Open
    </td>
    
    <td>
      7
    </td>
  </tr>
  
  <tr>
    <td>
      Further Consideration Required
    </td>
    
    <td>
      78
    </td>
  </tr>
  
  <tr>
    <td>
      Last Call
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr>
    <td>
      Closed, to be incorparated in COR1
    </td>
    
    <td>
      175
    </td>
  </tr>
  
  <tr>
    <td>
      Closed, to be incorparated in AMD11
    </td>
    
    <td>
      24
    </td>
  </tr>
  
  <tr>
    <td>
      Closed without Action
    </td>
    
    <td>
      9
    </td>
  </tr>
</table>

Wow. Will now start to go through the details. Will take time. But with that amount of defects in place, how can anyone safely implement OOXML according to IS29500?
