---
title: 'NetworkManager &#8220;Connection Established&#8221; annoyance SOLVED'
date: 2009-04-06T14:17:19+00:00
author: jwildeboer
layout: single
categories:
  - OpenMisc
  - Tips & Tricks & Fixes
tags:
  - Fedora
  - OpenKnowledge
---
So when you are running a GNOME Desktop and you frequently change locations/network connections, you might get annoyed by the &#8220;Connection Established&#8221; popup/balloon tip.

I did 🙂

So I solved it. At least with Fedora 10. Shoudl work the same with Ubuntu etc.

For the graphical guys:

  * Start Applications -> System Tools -> Configuration Editor (which really is gconf-editor)
  * Find the Entry /apps/nm-applet/
  * Add a new key called  **disable-connected-notifications**, type boolean and set it to TRUE
  * Restart NetworkManager (open a root shell, type **service NetworkManager restart**)
  * DONE

Whenever your network changes, the applet will show its cool anomations but will not bother you with popups.
