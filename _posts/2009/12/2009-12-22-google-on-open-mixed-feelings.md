---
id: 517
title: 'Google on Open &#8211; mixed feelings'
date: 2009-12-22T11:02:06+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=517
permalink: /2009/12/google-on-open-mixed-feelings/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
So Jonathan Rosenberg, Senior Vice President, Product Management, teaches us all on his understanding of &#8220;open&#8221; in various fields in [this article](http://googleblog.blogspot.com/2009/12/meaning-of-open.html) over at the official Google Blog. I gather that he reflects a sort of canonical defintion that somehow is used or should be used inside Google.

It leaves me with mixed feelings.

It contains a lot of good stuff on open standards (yet fails to point to a proper definition), it tells us about Open Source and Google (and contains the typical claim that they are the &#8220;biggest contributor to open source&#8221; &#8211; a claim used inlfationary across all tech companies).

But it also contain stuff that really surprises me in negative ways. Let&#8217;s go through some stuff and you, dear reader, decide yourself:

> If the GNU C compiler that I&#8217;m using has a bug, I can fix it since the compiler is open source. I don&#8217;t have to file a bug report and hope for a timely response.

Well &#8211; you should file a bug report and put your fix into that bug report so the whole community of GCC users and devs can have a better solution &#8230; private fixes == private fork == non-open &#8230;

> If existing standards aren&#8217;t as good as they should be, work to improve them and make those improvements as simple and well documented as you can. Our top priorities should always be users and the industry at large and not just the good of Google, and you should work with standards committees to make our changes part of the accepted specification.

The problem here is a seemingly ignorant attitude towards &#8220;upstreaming&#8221; modifications. Just as with the GCC example, Mr. Rosenberg seems to favour private forks of standards to gain a competitive advantage and treat the upstreaming as a nice-to-do afterwards. In my world, you would \*first\* try to upstream your modifications (to make sure you get sustainable support) and \*secondly\* base your product on the modifications.

> Today&#8217;s open source goes far beyond the &#8220;patent pooling&#8221; of the early auto manufacturers,

Patent-pooling creates a \*closed\* community &#8211; nothing open or competitive. See how GSM patents are used to deliberately hinder competitors to enter the market. Same with MPEG patents.

Also Open Source in general has \*never\* created a patent pool. Open Source has \*always\* opposed software patents.

&#8220;Others can take our open source code, modify it, close it up and ship
  
it as their own.&#8221;

So how is \*that\* open?

Don&#8217;t get me wrong, there are some good things in that article, but in general it leaves me with mixed feelings.
