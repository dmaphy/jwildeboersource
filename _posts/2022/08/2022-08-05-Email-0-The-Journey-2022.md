---
title: E-Mail Done My Way, Part 0 - The journey
categories:
  - Tips & Tricks & Fixes
tags:
  - smtp
  - postfix
  - dovecot
  - letsencrypt
  - SPF
  - DKIM
  - DMARC
header:
  image: mh/mh003.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 108771178158242761
---

**Warning. This whole series is not a simple HOWTO. This series is about how I run my mail server. I go through the configuration, line by line and explain. It's not going to be a simple Copy/Paste to run your own mail server. You have been warned.**

This will be at least a three part series. Maybe more. We will see.

 E-Mail, while being one of the oldest services on the internet, is a complex beast to tame. You have to be careful at every step of setting it up. Failure can cost you dearly. Open relays, blacklists, Denial of Service. There be dragons. These are the building blocks I use and will explain:

| 0. | [The Journey](/2022/08/Email-0-The-Journey-2022/) - **The basics and outlook (on the series, not the Microsoft mail client ;)** |
| 1. | [Postfix](/2022/08/Email-1-Postfix-2022/) - the in and out, so to say. The robust, battle-hardenened connection point for other mail servers on the internet to send emails to and receive emails from your domain(s). Also known as the MTA, the Mail Transfer Agent. |
| 2. | [Dovecot](/2022/08/Email-2-Dovecot-2022/) - where you and your users talk to to get emails to their mail client, be it your smartphone, a mail client on your computer or just even the command line. It's the IMAP server. |
| 3. | [DKIM/DMARC/SPF](/2022/09/Email-3-TheRest/) - Just having postfix and dovecot up and running isn't enough. We will also look at user authentication, letsencrypt certificates, DKIM, DMARC, SPF and the daily checks to make sure everything is humming along nicely. |
| 4. | [The final stuff](/2022/09/Email-4-Finals/) - How to make sure my e-mail server is happy and can do its job. Some simple checks, how to use fail2ban to keep bad servers and users away, checking log files, all those little things. |

### My setup

My setup is quite straight-forward. I have a single little virtual server that costs me a fiver a month. It talks IPv4 and IPv6. It has 1 CPU, 1 GB of RAM and 40GB of storage. More than enough, as e-mail is not that demanding, when done right.

It ofcourse runs Linux. What else? As I am a sucker for stability and reliability (and a Red Hatter), it runs Red Hat Enterprise Linux 8. A very basic install. I usually interact with it through ssh.

This little server manages e-mail for all of my 25 domains. So that's the first thing. Multi-domain. Adding new domains is quite simple. Or deleting them, when they expire. I don't have that many real e-mail accounts. It's mostly aliases and forwarding to my main accounts.

Now this might be a surprise to some of you - that server is at OVH. In the past, that would have been a big no-no. OVH IP addresses are blacklisted! Not trustworthy! So much spammers! Turns out - that isn't true anymore. Thanks to things like SPF, DKIM, DMARC you can run your mailserver there without problems. I can happily send and receive e-mails to Google, Microsoft, Yahoo etc. without problems. Some old prejudices are just that - old and outdated prejudices. It's 2022 :)

I already hear the professionals scream: "You need more than one server! It's a single point of failure! You need a cluster and load balancing and syncing and backups" - and yes, you are right. I should. But that opens up so much more cans of worms, that I have decided long ago to accept the risks. And it has worked really well for the past 10 years. So there's that. Don't be me. Be better :)

With that said, back to the setup. I have already explained [my DNS setup](/2022/07/DNS-done-my-way/) for all of my domains, which also explains the basics of making sure all e-mail for all domains go to one single server, with the correct entries for SPF, DKIM, DMARC.

This setup also means I can collect e-mails for all domains via this one single host, making it really easy to spin up special e-mail addresses as needed.

### Things I DON'T do

I am a command line guy. I don't care about webmail. Databases. Everything is in config files and delivers really just the absolute basics. SMTP, IMAP. For a handful of users that I personally know and can support. I care a lot about privacy and security, so that is more important than flashy web pages and over the top automation. It has to work. And I must understand every single step on the way. That is both a limitation but also a truly humble way to Get Things Done.

### The TL;DR before we start

So what do we have? A single machine with a single hostname, that happily sends and receives e-mail to many domains for just a fiver per month. Nice. So let's move on to the next episode:

[Part 1: Postfix](/2022/08/Email-1-Postfix-2022/)