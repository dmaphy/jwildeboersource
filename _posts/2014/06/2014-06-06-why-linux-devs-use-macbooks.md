---
id: 1614
title: Why (Linux-)devs use Macbooks
date: 2014-06-06T14:11:18+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1614
permalink: /2014/06/why-linux-devs-use-macbooks/
categories:
  - OpenMisc
tags:
  - Linux
  - MacOS
---
Just read an <a href="http://mjg59.dreamwidth.org/31714.html " target="_blank">article</a> by Matthew Garrett on why developers use Macs.

**My take:**

I think it is also correct to say that the good ole &#8220;workstation&#8221; metaphor of having a separate machine for development is being replaced. Now that computers are ubiquitous parts of the developers life, we prefer to have an all-in-one machine for that.

No more separate work or personal machines. We are looking for a &#8220;life-machine&#8221;. The laptop as a companion that is always at our hands. And as developers we need more than just a tablet or a mobile phone.

And I think that&#8217;s why many people prefer OS-X over Windows or Ubuntu/Fedora. For everyday tasks as email, picture stuff, booking flights, doing taxes etc. OS-X definitely offers a good solution. And being UNIX-y enough to be used in a Linux delpoyment context, you get a good compromise.

Discussion also <a href="https://plus.google.com/u/0/+jwildeboer/posts/9S9w4tjeYew" target="_blank">here on Google+</a>
