---
id: 1619
title: 'Thank you, Tesla Motors For The Patents, but &#8230;'
date: 2014-06-13T12:48:08+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1619
permalink: /2014/06/thank-you-tesla-motors-for-the-patents-but/
categories:
  - OpenMisc
tags:
  - Tesla
  - Patents
---
Here&#8217;s the thing. **Elon Musk doesn&#8217;t trust the patent system** to protect his inventions. So instead of filing for more, he will simply not file at all and keep his inventions secret. The stuff that already got patented thus is already considered lost by him so it is safe to &#8220;open source&#8221; them all.

While the move itself is a good one for the makers and hackers out there, it shows the fundamental problem of the patent system: It doesn&#8217;t do its job.

It doesn&#8217;t protect and foster innovation as patent trolls and patent pools have subverted that part of the deal.

It doesn&#8217;t make knowledge available as patent claims have become so broad and vague that those &#8220;skilled in the art&#8221; do not learn from patents &#8211; which is the CORE task of the patent system. This is especially true for software patents. Trust me, as a programmer I know what I am saying here. No software patent has ever taught me something.

The patent system has become a caricature of its intended purpose and delivers the exact opposite of what it should do,

Small to medium sized businesses don&#8217;t file patents at all most of the time. It&#8217;s prohibitively expensive, it doesn&#8217;t create value (except for trolls and lawyers) and it doesn&#8217;t protect the innovator.

So Elon Musk has decided to drop patents from his world. [He clearly states it](http://www.businessinsider.com/elon-musk-patents-2012-11):

> &#8220;We have essentially no patents in SpaceX. Our primary long-term competition is in China,&#8221; said Musk in the interview. &#8220;If we published patents, it would be farcical, because the Chinese would just use them as a recipe book.&#8221; 

I think he uses the same reasoning for Tesla. So while the opening up of Teslas patents is a good move, we will clearly see added secrecy to protect the real innovation happening at Tesla.

And I cannot blame Musk. I blame the broken patent system, the trolls, the courts and a clear lack of vision by our political leaders to promote the needed changes.﻿

Let&#8217;s discuss [over at Google+!](https://plus.google.com/112648813199640203443/posts/Z4RLPpGhsqG)
