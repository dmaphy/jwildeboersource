---
layout: single
title: Testing this Jekyll and github stuff
---

Alright. I am not really sure where I am going to take this, but I now have a static blog here. I might move my existing blog over or I might keep this blog limited to stuff related to my mediocre programming stuff, mostly around ESP8266, Home Automation, MQTT, Node-Red etc.

We will see. For now I am quite happy that stuff works and I can update/post from everywhere, prepare posts offline and correct stuff on the fly. Freeing my poor little server from all the useless attacks against my WordPress install is however quite an option.

Next: Try to integrate GooglePlus for comments :-)
