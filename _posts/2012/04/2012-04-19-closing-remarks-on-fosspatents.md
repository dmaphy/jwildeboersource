---
id: 1532
title: Closing remarks on FOSSpatents
date: 2012-04-19T13:35:22+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1532
permalink: /2012/04/closing-remarks-on-fosspatents/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Florian Müller (or Mueller when he is quoted in english articles), a self-acclaimed &#8220;expert&#8221; on software patents and nowadays quite a mouthpiece for FRAND licensing, has always been someone I had a love-hate relationship with. We fought together against software patents in the 2003-2005 years. And while he lost interest and left the stage with a loud announcement that he will never ever work on software patents again, decided to switch to soccer lobbying etc, I was hired by Red Hat, which was (and is) a dream come true and I continued my fight against &#8220;IP&#8221;-extremism.

Out of the blue Florian came back to the spotlight a few years ago, fighting appartently for David against Goliath in the Turbohercules case (which later turned out to be a company that was funded at least partly by Microsoft), fought for (former) MySQL&#8217;s Monty Widenius to stop the SUN/Oracle merger (where Monty later supposedly admitted that Florian was the wrong guy for the job), admitted that he did some &#8220;strategic&#8221; consulting for Microsoft, shouted about billions and billions at stake when Oracle sued Google, admitted he worked on a research paper for Microsoft to explain that FRAND is somehow compatible with Free Software and Open standards &#8211; or in short: He came back with a flurry of stuff that seemed unrelated but not exactly on the same side as my fights.

I accused him at various times that a lot of what he claimed is FUD, blown out of proportion etc. But who am I to criticise Europe&#8217;s biggest expert? 😉

Now the court case in Oracle v Google has started. And after all the shouting about billions and billions at stake I was sure that Florian would either be in the court room or at least report the hell out of that case. As, after all, it was him who reported on it for a long, long time. But what did I see? Nothing.

But I give him credits for finally explaining it all himself [here](http://www.fosspatents.com/2012/04/oracle-v-google-trial-evidence-of.html
)

> &#8220;Oracle has very recently become a consulting client of mine. We intend to work together for the long haul on mostly competition-related topics including, for one example, FRAND licensing terms.&#8221;

Where is my surpised face again? Ah, there it is!

This information puts his reporting of the past months in a new light IMHO. He clearly says that 

> &#8220;When Oracle and I started talking about areas in which I could provide analysis, we thought that the Google litigation was going to be over by the time we would work together.&#8221;

So it is safe to assume it wasn&#8217;t last week. I have always said that in my personal opinion Florians &#8220;analysis&#8221; is biased. Seems I wasn&#8217;t that wrong 😉 All journalists that have used his public and non-public information should take note that they are talking to someone that works (and/or has worked) for both Microsoft and Oracle and thus is not exactly an independent source or expert.

That&#8217;s all.
