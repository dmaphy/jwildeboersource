---
id: 1558
title: Bettina Wulff schmeckt lecker nach Hähnchen -For the Lulz
date: 2012-09-11T13:14:02+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1558
permalink: /2012/09/bettina-wulff-schmeckt-lecker-nach-hahnchen-for-the-lulz/
aktt_tweeted:
  - 1
categories:
  - OpenFun
---
This is a blog entry that you can safely ignore. The background story is quite interesting. Wife of former german Bundespräsident sues Google for adding keywords to the search bar when typing her name. There were (wrong) rumours of her having been an escort girl etc.

Long story short &#8211; we now try to Googlebomb her name to auto-add &#8220;tastes like chicken&#8221; when you type her name.

Shoutout to <a href="http://www.senadpalic.de/?p=410" title="Bettina Wulff schmeckt lecker nach Hähnchen" target="_blank">Senad Palic</a> for bringing this to my attention!
