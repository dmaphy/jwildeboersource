---
id: 1525
title: 'The Patent System &#8211; why it fails.'
date: 2012-02-13T09:45:19+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1525
permalink: /2012/02/the-patent-system-why-it-fails-loooong/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
What started as a <a href="https://plus.google.com/u/0/112648813199640203443/posts/g1xvVpXwZgr" title="Googleplus entry" target="_blank">post on Googleplus</a> turned into quite a nice braindump of my struggle with the current patent system. Hence I decided to also turn it into a blog entry. Please do read the comments on googleplus as they contain a lot of additional insight and counterarguments.

**HISTORY**

The fundamental concept of the patent system (similar to the fundamental concept of the copyright system as both date back to queen Anne) is sound. A limited monopoly in exchange for a full disclosure of the invention. This concept was created to give an incentive to sharing knowledge and avoid knowledge disappearing with the death of the inventor by him/her keeping hsi/her secret.

**THE DILEMMA**

The perversion ATM is that both sides of the equation fail. The limited monopoly of max 20 years (note many patents don&#8217;t even make it that long as the fees are quite hefty) is extremely problematic as it allows the patent holder to not license at all, especially if he/she isn&#8217;t doing anything with the patented technology, thus using the patent system in a fundamentally wrong way, and &#8211; FAR more important &#8211; the full disclosure to anyone &#8220;skilled in the art&#8221; fundamentally fails.

I have read many patents. And I have not learnt much from them. The patent language &#8220;a method comprising of a computer readable medium doing SomeWeirdLanguage&#8221; etc does not teach me anything.

**CONCLUSION**

So when both sides of the equation fail in society (not the market, patents are a deal between society and creators, not a deal in the market, a mistake many people make) the patent system is NOT doing what it is meant to do.

Now can we get back to the original deal? I doubt it. Reforms will not suffice for that.

**THE POSITIVES**

OTOH, especially with Open Standards and Open Source/Free Software we have an alternative system that delivers on the fundamental promise that once created the patent system.

Open Standards and FOSS promote science and the useful arts by offering full disclosure (source code, specifications) to anyone. And even for free. The limited monopoly is not needed anymore. Someone skilled in the art will find a job and can promote his solutions without the need for a monopoly.

**FUNDAMENTAL CHANGES**

Why is that? Because in the old days the investments needed to get from an idea to a product one could offer in the market were very high. So the monopoly was needed to at least give the inventor a way to recoup these costs &#8211; thereby accepting the possible abuse as we have seen over many years. But the fact that the patented knowledge became public domain after 20 years was outweighing that risk in the eyes of the governments making the laws.

Nowadays the investments are low, close to none in the case of pure software patents and the distribution costs of software are effectively zero &#8211; so the need for a limited monopoly becomes harder to justify. See the record/movie industry for a perfect example of this fundamental change working its way through society.
