---
id: 1522
title: 'FRAND fairy tales: Google, Motorola, Microsoft and numbers.'
date: 2012-02-09T15:08:48+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1522
permalink: /2012/02/frand-fairy-tales-google-motorola-microsoft-and-numbers/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
First read this:

[CNN: Google Wants Huge Royalties Every Time Apple Sells An iPhone](http://bit.ly/zdJZyL)

Now let&#8217;s ignore for the moment the obvious wrongness wrt Android copying iOS etc and instead focus on the claim that 2.25% is not FRANDish enough according to the self-acclaimed expert in thsi field, Mr. Florian Mueller.

This same Mr. Mueller is working on a &#8220;research project&#8221; to find out how FRAND is the best way for the IT world etc. Hint: that research is payed by Microsoft 😉

Hm. A simple search and replace gives me:

**Microsoft wants huge royalties every time {HTC, SAMSUNG, Motorola, Huawei, &#8230; } sells an Android Phone.&#8221;** 

# #justsayin

MSFT wants between 5-15US$ per device according to various sources. With a unit price of, say, 200 US$ this means a range between 2.5% and 7.5%. Which AFAICS is a bit more (up to 3x) as what Motorola asks for. Now do the math with a more realistic unit price between 80-150 US$ to see who is the bully here.

That&#8217;s why I would even go as far as saying that 2.25% ratio is more fair, reasonable and non-discriminatory compared to a fix sum regardless of the unit price. 😉
