---
title: 'Slic3r on RHEL6, 64 bit &#8211; HOWTO'
date: 2012-08-27T13:32:11+00:00
author: jwildeboer
layout: single
categories:
  - Tips & Tricks & Fixes
---
As one of my hobbies is 3D printing, I have built an <a href="http://www.emakershop.com/emaker-huxley" title="eMaker Huxley" target="_blank">eMaker Huxley</a> 3D printer last year. It is fun, teaches me a lot and I try to keep the setup up2date. So recently I upgraded the <a href="http://reprap.org/wiki/Sanguinololu" title="Sanguinololu" target="_blank">Sanguinololu</a> to use a 1284P CPU and put <a href="https://github.com/ErikZalm/Marlin/" title="Marlin Firmware" target="_blank">Marlin</a> on it. I added a LCD Display and a rotary encoder so it now runs completely standalone.

To create the files to print, you need to slcie/skein 3D models to GCODE. I started with Skeinforge for that but have switched to Slic3r recently.

I am a big fan of <a href="http://www.slic3r.org" title="Slic3r" target="_blank">Slic3r</a> and like to keep up with the latest devlopments. So I typically build it myself. In order to do that, you need to have quite some requirements satisfied. Here the short form for building Slic3r on Red Hat Enterprise Linux 6, 64bit &#8211; the OS on my current laptop:

First some RPM packages that are needed:

`sudo yum install git wxBase wxGTK wxGTK-devel wxGTK-gl dbus-devel expat-devel cpan`

As Slic3r is a perl program, we need quite some CPAN stuff:

`sudo cpan Boost::Geometry::Utils Math::Clipper Math::ConvexHull Math::Geometry::Voronoi Math::PlanePath parent Moo Module::Build::WithXSpp dbus-1 XML::Twig`

Now let&#8217;s fetch Slic3r:

`git clone https://github.com/alexrj/Slic3r.git`

And now we should be able to build Slic3r:

`perl Build.PL`

Future updates should be a simple

`git pull; perl Build.PL`

Time to slice some files from <a href="http://www.thingiverse.com" title="Thingiverse" target="_blank">thingiverse</a>.com!
