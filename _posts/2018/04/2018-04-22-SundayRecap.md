---
layout: single
title: SundayRecap
date: '2018-04-22 18:45:06 +0159'
categories:
  - Personal
tags:
  - TCP/ID
  - Jekyll
  - GitHub
  - LetsEncrypt
  - iOS
  - MinimalMistakes
published: true
permalink: 2018/04/SundayRecap
header:
  image: mh/mh001.jpg
---

Time to revive this blog. I let it linger in limbo for too long. Running on a WordPress instance I set up 9 years ago (but I kept it up2date, vigorously). I looked at it with a growing need to do SOMETHING. Write new entries, close it down, move it somewhere else.

Which I did over the past few days. I am a paying [Github](Github.com) user, so I dived a bit deeper into github pages, Jekyll, building a workflow. Looking through a lot of guides, trying around a bit. Now I finally made the jump. All my old entries are still here, the old Permalinks should still work. But I lost all comments in the migration, which I knew would happen. Integrating comments in a static site is a problem. It will be no comments for the foreseeable future. Feel free to comment/discuss on Twitter, LinkedInand G+, I've added buttons to every post to do just that. I am using the most wonderful [Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/) theme and I am only starting to explore its possibilites. Have a look and make sure you show your appreciation to its author at his [donation page](https://mademistakes.com/support/) just as I did today.

## My new workflow - Mobile first, the geeky way!

TL;DR [Editorial](http://omz-software.com/editorial/) on iOS, [Working Copy](https://workingcopyapp.com/) on iOS, git push to github, Jekyll via Github Pages, published on [this blog](https://jan.wildeboer.net). Mostly based on [this guide](https://www.thecave.com/2017/04/21/how-i-post-to-my-jekyll-site-using-my-iphone/) by Kirby Turner. This allows me to catch any thoughts and jot them down in Editorial, finishing them up as I find time and publish it all with one single press of a button. Neat! Extra bonus: my custom domain has automatically been given a [Let's Encrypt](letsencrypt.org) certificate by github, so everything is securely transported to you, my dear reader!

I’m still struggling to find a good way to automatically insert/rescale/upload pictures, but first I need to learn more basics and maybe make it all look a bit more attractive. this is my new, old home and I will try to give you beautiful content on a regular basis. With that, time for a decent beer to celebrate.

On this warm and sunny Sunday, in a Bavarian beergarden … [^1]


[^1]: [Lindengarten](https://www.lindengarten.eu), to be precise :)