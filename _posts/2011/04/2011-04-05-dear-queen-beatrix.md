---
id: 1456
title: Dear Queen Beatrix,
date: 2011-04-05T11:54:58+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1456
permalink: /2011/04/dear-queen-beatrix/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
**UPDATE 2** Rop Gonggrijp blogs, mentions this petition [here](http://rop.gonggri.jp/?p=475). Explains there is no extradition request ATM and urges all to tone down. 

**UPDATE:** Webwereld reports on the petition and this blog entry [here (in dutch)](http://webwereld.nl/nieuws/106259/-beatrix-moet-gonggrijp-beschermen-tegen-vs--.html#utm_source=front_current_3&#038;utm_medium=website&#038;utm_campaign=ww).


  
**Sign my petition [here](http://www.petitiononline.com/RopGDef/). Spread the word.**

I am a proud citizen of the Netherlands. A country filled with smart, pragmatic people. A country that is proud of its liberalism, its openness and freedom. Some fellow citizens are true heroes. One of them is [Rop Gonggrijp](http://en.wikipedia.org/wiki/Rop_Gonggrijp). His fight for freedom in both the virtual and real world is filled with amazing legends, myths and clashes with autorities. But as a real proud citizen of your country, he never gave up. He fights for true values. The very values that our kingdom represents.

It is with anger and disbelief that I now read that the current interior minister of your government is willing to give up the freedom of Rop Gonggrijp to please the United States. As you can read [here](http://webwereld.nl/nieuws/106249/nederland-staat-open-voor-uitleveren-gonggrijp---update.html#utm_source=front_current_head&#038;utm_medium=website&#038;utm_campaign=ww), Uri Rosenthal has no problem to extradite Rop Gonggrijp to the United States should they so desire.

And why? Because he helped [Wikileaks](http://wikileaks.ch/) to publish the [truth](http://www.collateralmurder.com/). Because he helped the truth to be put in the spotlight of public scrutiny. A truth that is tough to accept, but true to his nature, Rop Gonggrijp defended the freedom to tell the truth.

You, dear Majesty, should be proud of him. His fight for freedom and truth reflect the values that your Majesty asks from her citizens. Therefore I urge you, dear Majesty, to stop this shameful actions from your government. I ask you to step up and declare that Rop Gonggrijp is fighting for the right cause. I ask you to declare that you protect him, as you would protect any other dutch citizen.

This promise from your side is printed in every dutch passport. I will quote the english version, copied straight from my passport:

> In the name of Her Majesty the Queen of the Netherlands, Princess of Orange-Nassau, etc., etc., etc, the minister of foreign affairs requests all authorities of friendly powers to allow the bearer of the present passport to pass freely without let or hinderance and to afford the bearer every assistance and protection which may be necessary.

If you feel the same, you can sign my petition [here](http://www.petitiononline.com/RopGDef/). Spread the word.
