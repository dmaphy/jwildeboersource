---
id: 1380
title: 'Update on killassange.com, domainsbyproxy #IFWL'
date: 2011-01-14T11:51:24+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1380
permalink: /2011/01/update-on-killassange-com-domainsbyproxy-ifwl/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Continuing my efforts to alert registrars and hosting providers on IMHO defamatory domain names, which [started here.](https://jan.wildeboer.net/2011/01/wrt-killjulianassange-com-killassange-com-and-godaddy/)

The domain killassange.com has no DNS entries at the moment, meaning you cannot reach it. Which is a good thing and thanks to HPCISP.com for reacting so timely and solving this matter.

However, the domain itself [still exists](http://whois.domaintools.com/killassange.com) and can become active at any time. Hence my request to domainsbyproxy.com, the company the registered the domain on behalf of their customer through godaddy, as revealed by the whois data.

domainsbyproxy.com offers a [webform](https://www.domainsbyproxy.com/ClaimForm.aspx?Type=3) for complaints, which I used to send this request:

> As documented in my blog post at 
> 
> <blockquote data-secret="j1Q8h6Lqtw" class="wp-embedded-content">
>   <p>
>     <a href="https://jan.wildeboer.net/2011/01/wrt-killjulianassange-com-killassange-com-and-godaddy/">WRT killjulianassange.com, killassange.com and godaddy</a>
>   </p>
> </blockquote>
> 
> 
> 
> It is my honest opinion that the domain kulljulianassage,com is unacceptable. Your Domain Name Proxy Agreement at https://www.domainsbyproxy.com/policy/ShowDoc.aspx?pageid=domain_nameproxy states in 4 &#8211; iv &#8211; G:
> 
> G. If it comes to DBP&#8217;s attention that You are using DBP&#8217;s services for purposes of engaging in, participating in, sponsoring or hiding Your involvement in, illegal or morally objectionable activities, including but not limited to, activities which are designed, intended to or otherwise: 
> 
> [&#8230;]
> 
> 2. Defame, embarrass, harm, abuse, threaten, or harass third parties; 
> 
> [&#8230;]
> 
> In my humble opinion the domain name killassange.com is without doubt in this definition. Therefore I urge you take the appropriate action and remove this domain name from the registry, inform the owner of this domain of your actions and update me via e-mail in the next 48 hours
> 
> This complaint has been documented at my blog at <https://jan.wildeboer.net/2011/01/update-on-killassange-com-domainsbyproxy-ifwl/> and I would like to publish your reply at ethe same place. Should you object to this, please state so in your reply.
> 
> Jan Wildeboer

The popup after submit told me:

> Thank you for your submission. Your tracking ID number is 575601. Please refer to this ID number when communicating with our office (generalmanager@domainsbyproxy.com) regarding this report. Please allow up to 3 business days for us to contact you about this claim. 

Updates will be added to this entry as they come to my attention.
