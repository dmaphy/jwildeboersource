---
id: 1365
title: WRT killjulianassange.com, killassange.com and godaddy
date: 2011-01-10T
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1365
permalink: /2011/01/wrt-killjulianassange-com-killassange-com-and-godaddy/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
[UPDATE 2011-01-13 12:30 CET Trying to get at the domain owner of killassange.com via domainsbyproxy in [seperate post](https://jan.wildeboer.net/2011/01/update-on-killassange-com-domainsbyproxy-ifwl/)
  
[UPDATE 2011-01-12 18:20 CET: killassange.com seems to have no DNS entry ATM, therefore not resolving. Success?]
  
[UPDATE 2011-01-11 21:54 CET: killassange.com and killjulianassange.com both up and running, no reply from godaddy so far]
  
[UPDATE 2011-01-11 11:44 CET: Seems the domain julianassangemustdie.com has been deleted by its owner. See [Godaddy WHOIS entry](http://who.godaddy.com/WhoIs.aspx?domain=julianassangemustdie.com&#038;prog_id=godaddy)
  
[UPDATE 2011-01-11 11:33 CET: Seems killjulianassange is down ATM. Could be simple change though.]
  
[UPDATE: I send an updated mail with the domain julianassangemustdie.com added and clarified some aspects. New version reproduced here, old version archived in this same post.

I have just sent this mail to abuse@godaddy.com and urge you to do the same:

> Dear Godaddy,
> 
> as instructed by your twitter account, I hereby request to check if the domain names registered by godaddy on behalf of your customers
> 
> * killassange.com
> * killjulianassange.com
> * julianassangemustdie.com
> 
> are in violation of your terms of use.
> 
> In my personal opinion they are in violation of your terms of use as stipulated at
> 
> http://www.godaddy.com/Agreements/ShowDoc.aspx?pageid=TOU&#038;ci=20801&#038;app_hdr=0
> 
> more specifically 4 iv which I partly quote verbatim here:
> 
> * Promotes, encourages or engages in hate speech, hate crime, terrorism, violence against people, animals, or property, or intolerance of or against any protected class;
> 
> Therefore I kindly request to take the appropriate action and inform the owner of these domains about your findings.
> 
> I also kindly request to be informed of the outcome in the next 14 days.
> 
> I will continue to check if these domain names are active and which content is hosted.
> 
> I have published this email on my blog at 
> 
> <blockquote data-secret="BSqetRT17d" class="wp-embedded-content">
>   <p>
>     <a href="https://jan.wildeboer.net/2011/01/wrt-killjulianassange-com-killassange-com-and-godaddy/">WRT killjulianassange.com, killassange.com and godaddy</a>
>   </p>
> </blockquote>
> 
> 
> 
> I would like to publish your response at the same place, as my readers and twitter/identi.ca followers have indicated their interest in the resolution of this issue.
> 
> Should you not want to see your reply published, please explicitly state that in your reply.
> 
> Kind regards,
> 
> Jan Wildeboer 

This is the old version I sent yesterday.

> Dear Godaddy,
> 
> as instructed by your twitter account, I hereby request to check if the domain names
> 
> * killassange.com and
> * killjulianassange.com
> 
> are in violation of your terms of use.
> 
> In my personal opinion they are in violation of your terms of use at
> 
> <http://www.godaddy.com/Agreements/ShowDoc.aspx?pageid=TOU&#038;ci=20801&#038;app_hdr=0>
> 
> more specifically 4 iv:
> 
> * Promotes, encourages or engages in hate speech, hate crime, terrorism, violence against people, animals, or property, or intolerance of or against any protected class;
> 
> Therefore I kindly request to take the appropriate action and inform the owner of these domains about your findings.
> 
> I also kindly request to be informed of the outcome in the next 14 days.
> 
> I will continue to check if these domain names are active and which content is hosted.
> 
> Kind regards,
> 
> Jan Wildeboer 

I will update this post should I receive a reply.
