---
id: 1339
title: '[SOLVED] Joindiaspora and me &#8211; #FAIL all over the place'
date: 2011-01-04
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1339
permalink: /2011/01/joindiaspora-and-me-fail-all-over-the-place/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
**UPDATE My problems are fixed! I am impressed. Finally I can use Diaspora again. Thanks to the devs who finally looked at Bug #734 and fixed my problems.**

So I am one of those with an account at joindiaspora. And I have some requests by other users to become my contact.

![Diaspora1](/images/2011/01/DiasporaStart.png)
*Screenshot of homepgae with requests*

Which is nice! But since 2 weeks, I cannot accept these requests, I cannot read notifications, I cannot use anything but the homepage. Because everytime I click on the requests, on the notifications, on my aspects, I get a nice Error 500:

![Diaspora1](/images/2011/01/DiasporaFail.png)
*Diaspora error message*

I found out after a bit of thinking and searching that the root cause probably is in [this bug report:](http://bugs.joindiaspora.com/issues/734)

> Not sure, but I think it&#8217;s because someone who invited me has deleted their account.

Which could make sense. If someone sends me a request and deletes his account before I accept the request, the now orphaned request seems to cause trouble. Ofcourse this means that the current process of deleting an account in diaspora is a bit sloppy as it seemingly doesn&#8217;t care about such orphaned requests. With all that in mind, some other brave soul with is own diaspora node tried my guess and &#8211; guess what &#8211; it turns out to be reproducible:

> &#8220;I managed to reproduce the error on my local Diaspora installation by deleting the person from whom the request comes from manually from MongoDB. I get exactly the same error so this I probably the cause.&#8221;

OK, so we have the root cause nailed down, we have a bug report. Now the only thing needed is a developer who fixes it and pushes the fix to joindiaspora.com.

And that is where we are. Unfortunately nothing seems to happen. No developer takes the bug, no one reacts. This bug renders my account effectively useless as I can do nothing besides doing a reload on the homepage. As soon as I switch to aspects, try to answer requests or even read my notifications . error 500.

I do hope this blog entry might be read by one of the diaspora devs. Please fix this. I am ready to help. But you need to react on your won bugtracker first. ATM this is a #FAIL situation. 16 days of an open bug and not a single action from the devs.

To those of you that sent me requests and wonder why I don&#8217;t act &#8211; here you are.
