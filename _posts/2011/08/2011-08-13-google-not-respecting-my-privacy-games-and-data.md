---
id: 1491
title: Google not respecting my privacy? Games and data
date: 2011-08-13T13:33:11+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1491
permalink: /2011/08/google-not-respecting-my-privacy-games-and-data/
aktt_tweeted:
  - 1
flattrss_autosubmited:
  - 'true'
categories:
  - OpenMisc
---
So Google announced and introduced the games tab in Google Plus.

We can now all play angry birds and be really unproductive all day 🙂

BUT

Nothing is for free. Google claimed to repsect the privacy of its users &#8211; also to differentiate itself from Facebook.

So I was suprised to read [this](http://www.google.com/support/profiles/bin/static.py?page=guide.cs&#038;guide=1334138&#038;answer=1334259):

> Below are a few examples of how games may use the information that they request:
> 
> [&#8230;]
> 
> Ordered list of your people in your circles
>
> Google gives the developer an ordered list of people from your circles. This tells the game developer the people you are most likely to want to engage with in the game. The order is based on your interactions across Google. This information could be used by the game to present you with people to play the game with, including to invite to the game, and to send gifts and messages. 

This means that when a friend of mine plays a game and grants the permissions (which he has to or else he cannot play) **MY** data gets shared with the game publisher &#8211; **and I am not even asked nor can I opt-out**

This is exactly the opposite of respecting my privacy.

Google, this is #fail on a very high level.

**I hereby urge Google to:**

  * Make the data sharing an opt-in thing
  * Alternatively AT LEAST allow me to opt-out
  * And do make available to me a list of all game publishers that have access to my data without my explicit consent

I am also a bit unhappy that the text says &#8220;Below are a few examples [&#8230;]&#8221; &#8211; where can I get a complete list of data that gets shared?

**Update** [this page](http://www.google.com/support/+/bin/static.py?page=guide.cs&#038;guide=1334138&#038;answer=1386475) has a link that could shed some light on exactly which data gets shared, but alas, it&#8217;s a 404. Please fix this, google. It says:

> Within Games
> 
> When you play a game, you’re consenting to share information such as your name and profile picture with the game developer. This lets developers design better social games. Learn more about what information developers can access. 

But the link goes to [http://www.google.com/support/+/bin/answer.py?answer=](http://www.google.com/support/+/bin/answer.py?answer=) which seems to be a broken link.
