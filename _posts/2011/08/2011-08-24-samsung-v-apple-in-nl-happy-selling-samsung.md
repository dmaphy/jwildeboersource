---
id: 1513
title: 'Samsung v Apple in NL: Happy selling, Samsung!'
date: 2011-08-24T15:42:07+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1513
permalink: /2011/08/samsung-v-apple-in-nl-happy-selling-samsung/
aktt_tweeted:
  - 1
flattrss_autosubmited:
  - 'true'
categories:
  - OpenMisc
---
OK. So here the short form of what happened in the dutch court today. Apple has **LOST** all claims wrt design and copyright.

Apple has LOST all claims wrt the european patent [2098948](https://data.epo.org/publication-server/rest/v1.0/publication-dates/20110209/patents/EP2098948NWB1/document.pdf). The court thinks that the european patent [1964022](http://worldwide.espacenet.com/publicationDetails/description?CC=EP&#038;NR=1964022A1&#038;KC=A1&#038;FT=D&#038;date=20080903&#038;DB=EPODOC&#038;locale=en_EP) is worthless and will be thrown out in reexamination anyway as prior art has been shown by Samsung. The only thing that remains is the european patent [2059868](http://worldwide.espacenet.com/publicationDetails/description?CC=EP&#038;NR=2059868A2&#038;KC=A2&#038;FT=D&#038;date=20090520&#038;DB=&#038;locale=en_EP). And the claims of that patent can be circumvented in trivial ways.

Thus the court has decided that Samsung must fix this part (picture scrolling and showing parts of the next/previous picture) in the next 7 weeks. IF they fix it, so that the patent is not infringed upon, they are FREE TO SELL the Galaxy phones.

Secondly &#8211; the whole claims around the Galaxy Tab have been either dropped or have been deemed not relevant by the court. So the Galaxy Tab IS FREE TO SELL.

No matter what alarmists will tell you, net result is:

  * Samsung can continue to sell current Galaxy phones and must provide a trivial change to the picture gallery in the next 7 weeks.
  * Samsung can continue to sell the Galaxy Tab.
  * Apple has LOST all design and copyright related claims.
  * Apple has LOST the infringement claim on one patent and the court deemed a third patent broken anyway.

That&#8217;s all folks.

The court has used the Knight-Ridder Newspad as an example and explained that Apple failed to convince the court that the knight-Ridder Newspad was unknown in the important circles. I would say that&#8217;s a huge loss.

**UPDATE** Dutch Webwereld agrees with my interpretation [here](http://webwereld.nl/nieuws/107696/rechtbank-den-haag-verbiedt-galaxy-s--sii-en-ace.html). They also add that the problem with the one patent is limited to Android 2.3. Android 3.0 does not infringe and hence the Galaxy Tab is free.

For those that need to be convinced in detail, here the [verdict](http://dl.dropbox.com/u/27963165/KG%2011-0730%20en%2011-731%20Apple%20-%20Samsung.pdf) and my interpretation:

> 5.1.
> 
> Verbiedt gedaagden om na verloop van 7 weken en één dag na betekening van dit vonnis op enigerlei wijze, direct dan wel indirect, door het vervaardigen, in voorraad hebben, aanbieden, invoeren, in het verkeer brengen, verkopen en/of anderszins verhandelen van smartphones Galaxy S, S II en Ace, inbreuk te maken op het Nederlandse deel van EP 2.059.868; 

In 7 weeks plus one day from now Samsung is denied the right to manufacture, stock, offer, import, bring into market, sell or otherwise trade the smartphones Galaxy S, S II and ace IF they infringe on the dutch translation of the claims of EP2059868

If these phone DO NOT infringe on the claims, they can be sold, manufactured etc. without restriction.

And they can also continue to be sold in this &#8220;infringing&#8221; way for the next seven weeks plus one day.

So anyone that claims they cannot be sold is WRONG.

> 5.2.
>  
> Verbiedt gedaagden sub 2-4 om na verloop van 7 weken en één dag na betekening van het te wijzen vonnis op enigerlei wijze, direct dan wel indirect, door het vervaardigen, in voorraad hebben, aanbieden, invoeren, in het verkeer brengen, verkopen en/of anderszins verhandelen van smartphones Galaxy S, S II en Ace, inbreuk te maken op de buitenlandse
>  
> delen van EP 2.059.868; 

Same for the non-dutch parts of the patent.

> 5.3.
>  
> Gebiedt gedaagden aan eiseres een onmiddellijk opeisbare dwangsom te betalen van EUR 100.000 voor elke dag of gedeelte daarvan of, zulks ter keuze van eiseres, van EUR 10.000 per inbreukmakend product, waarop het aan gedaagden kan worden toegerekend dat de verboden zoals opgenomen onder 5.1 en 5.2 niet geheel of niet deugdelijk worden nageleefd; 

Should Samsung fail to fulfill the requirements set forth in 5.1 and 5.2, Apple will get 100.000 EUR per day or alternatively 10.000 EUR per device that remains in violation of 5.1 and 5.2 until Samsung has fixed it.

> 5.4.
>  
> verklaart dit vonnis tot zover uitvoerbaar bij voorraad;
>  
> 5.5.
  
> compenseert de kosten van de procedures tussen partijen in die zin dat iedere partij
>  
> de eigen kosten draagt,
>  
> 5.6.
>  
> bepaalt de in artikel 1019i Rv bedoelde termijn op zes maanden, te rekenen vanaf
>  
> de dag van deze uitspraak;
>  
> 5.7.
>  
> wijst het meer of anders gevorderde af. 

Typical closing stuff.
