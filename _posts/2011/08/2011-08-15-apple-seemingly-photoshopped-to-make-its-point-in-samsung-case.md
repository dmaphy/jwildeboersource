---
title: Did Apple photoshop a picture to make its point in Samsung case?
date: 2011-08-15T16:47:07+00:00
author: jwildeboer
layout: single
permalink: /2011/08/apple-seemingly-photoshopped-to-make-its-point-in-samsung-case/
categories:
  - OpenMisc
published: true
---
**DISCLAIMER: This post contains my personal opinion only.**

**UPDATE: You can now read en english version of the original dutch article [here](http://www.computerworld.com/s/article/9219189/Apple_s_evidence_may_be_flawed_in_European_Samsung_case)**

My good friends over at [webwereld have news](http://webwereld.nl/nieuws/107599/apple-levert-onjuist-bewijs-in-zaak-tegen-samsung.html) for you. And it is close to sensational IMHO.

The article is in dutch, which happens to be my native language. So first the TL;DR version:

  * Apple's german lawyer seems to have altered pictures of Samsungs Galaxy Tab 10.1 in its filing that lead to the injunction taking the Galaxy Tab from the market.
  * Dutch court will not decide before October 13th and Samsung is free to sell the Galaxy Tab in the Netherlands. Apple has agreed to not interfere.

Now that's quiet something. Let's delve in a little more detailed.

**The Altered Picture Thingy**

This is quite an accusation. On page 28 of the complaint that Apple filed in germany you can find a picture of the iPad2 next to a Galaxy Tab. They look almost identical, underpinning Apple's point that the Galaxy Tab is a simple copy of Apple&#8217;s &#8220;revolutionary&#8221; design.

![Samsung Apple 1](/images/2011/08/Apple1.png)
*Apple's picture from the complaint*

However &#8211; it isn&#8217;t. The aspect ratio has been altered. As the article explains, in the filing, both pictures indicate devices with an aspect ration of 1,36. In reality however the Galaxy Tab is 256,7 x 175,3 mm which leads to an aspect ration of 1,46.

So the correct picture is closer to this, as webwereld claims:

![Samsung Apple 2](/images/2011/08/Apple2.jpg)
*Webwereld version*

Quite a difference. Some people claim this might be based on the fact that Apple&#8217;s lawyer might have used pictures from a different, earlier prototype model. Even if that is true, it would make the whole thing worse. How can Apple ask for injunction based on a device that will never be available in the market?

No matter how Apple and its friends will try to spin this one, the injunction is at least fro this part based on flawed information.

Samsung would be well advised to point the german court to this obvious problem.

Also note that the (quite big) Samsung logo that is on the front of the Galaxy Tab has seemingly disappeared. Not even mentioning the fact that the Galaxy Tab does not have Apple&#8217;s iconic home button.

I went to Amazon and took product pictures for both products, used gimp to set them in the correct relation and voila, this is how it looks in reality:

![Samsung Apple 3](/images/2011/08/ipadvtab.jpg)
*Correct proportions*

And suddenly it isn&#8217;t that convincing anymore. 

**Happy Selling in the Netherlands!**

The second point is also quite interesting. As we all know, the injunction covers the european union **except** the Netherlands. This is due to the fact that there is a separate case going on in the Netherlands and the german injunction cannot override the proceedings of thi separate case.

Now in this case in the Netherlands the court has decided that Samsung is free to offer, distribute and sell the Galaxy Tab in the Netherlands at least until October 13th.

Apple (I guess with grinding teeth) has agreed to let this happen and refrain from any interfering actions like new cases etc.

So there you have it. Despite all the alarmist posts and articles, there is more to find out. I am really astonished that Apple&#8217;s lawyer made such a blatant and obvious mistake with the pictures and I would not be surprised if the german court will not be very amused about this.
