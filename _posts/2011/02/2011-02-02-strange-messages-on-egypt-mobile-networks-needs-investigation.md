---
id: 1388
title: 'Strange messages on #egypt mobile networks &#8211; needs investigation'
date: 2011-02-02T23:31:49+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1388
permalink: /2011/02/strange-messages-on-egypt-mobile-networks-needs-investigation/
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
_**EXEC SUMMARY:** Vodafone and Etisalat networks transport government scripted messages supporting Mubarak. These messages are not normal SMS. Seemingly they are so-called Premium-Rate Short Messages that are submitted by companies on behalf of their customer/users to multiple recipients. At the same time normal SMS in Egypt are blocked, so people in Egypt cannot send SMS themselves._

**CONFIRMED**: Vodafone states egyptian authorities forced them to broadcast messages supportive of Mubarak regime. 

People in Egypt have been getting these messages, despite an existing block on normal SMS.

![Egypt1](/images/2011/02/234806771.jpg)
*Premium Rated Short Message from "Egypt"*

![Egypt2](/images/2011/02/rqd.png)
*Premium Rated Short Message from "EgyptLovers"*

OK. So according to my sources, the translation is:

> Big demo starting this afternoon from Mustafa Mahmoud Square in Mohandaseen area to support Mubarak

![Egypt3](/images/2011/02/hf7fv.jpg)
*Second message sent*

Here the second message is the same and the first translates to:

> The military cares about your safety and security and it won&#8217;t use force against these great people. 

The message on etisalat shows &#8220;Egypt&#8221; as sender, the Vodafone one shows &#8220;EgyptLovers&#8221; as sender.

Both seem to be some kind of service message, not normal SMS. My contacts in Egypt confirm that they themselves cannot send SMS at all and they also tell me that not everyone has gotten these messages.

So questions arise:

**Who is behind this?**
  
Seemingly these are so-called [Premium-Rated Short Messages](http://en.wikipedia.org/wiki/SMS#Premium-rated_short_messages), which means they are sent by protocols like [SMPP](http://en.wikipedia.org/wiki/Short_message_peer-to-peer_protocol), not as normal SMS.

**Was the mobile network provider involved?**
  
I would conclude yes. Normal people cannot send SMS at the moment. These messages are posted using a different system, as I outlined above. These SMPP costumers typically pay to the providers to use the system. So Vodafone and Etisalat should be able to find out who is behind this &#8211; at least which company posted them.

Will update. Please comment if you can shed more light on this.

[UPDATE: Vodafone Statement:](http://www.vodafone.com/content/index/press.html)

**Thursday 3 February 2011**

> Under the emergency powers provisions of the Telecoms Act, the Egyptian authorities can instruct the mobile networks of Mobinil, Etisalat and Vodafone to send messages to the people of Egypt. They have used this since the start of the protests. These messages are not scripted by any of the mobile network operators and we do not have the ability to respond to the authorities on their content.
> 
> Vodafone Group has protested to the authorities that the current situation regarding these messages is unacceptable. We have made clear that all messages should be transparent and clearly attributable to the originator.

**Wednesday 2 February 2011**

> Vodafone is pleased to confirm that it has been able to reinstate data services in Egypt this morning, enabling our customers to access all internet sites. We are actively lobbying to reactivate SMS services as quickly as possible for our customers.

